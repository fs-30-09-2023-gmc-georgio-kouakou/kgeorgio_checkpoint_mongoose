const express = require('express');
const app = express();
const cors = require("cors");
console.log("App listen at port 5000");
app.use(express.json());
app.use(cors());

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/', {
    dbName: 'Checkpoint',
    useNewUrlParser: true,
    useUnifiedTopology: true
}, err => err ? console.log(err) :
    console.log('Impossible to connected to Checkpoint database'));


app.listen(5000);