import mongoose from 'mongoose';
const { Schema } = mongoose;

const PersonModel = new Schema({
    title: String, // String is shorthand for {type: String}
    age: Number,
    favoriteFoods: [String],
});

PersonModel.create(function (err, result) {
    title = 'Blabla',
    age = 20,
    favoriteFoods = ['Pizza', 'Burger', 'French Fries']
});

PersonModel.create(function (err, result) {
    title = 'Lorem Ipsum',
    age = 12,
    favoriteFoods = ['Foutou', 'Alloco']
});


PersonModel.save()

PersonModel.findOne({ title: 'Blabla' })

PersonModel.findOneAndUpdate({ title: 'Lorem Ipsum'}, {title: 'George'})

PersonModel.findOneAndRemove({ title: 'George'})
export default Person